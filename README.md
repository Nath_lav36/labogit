===
=== IFT-1902 PROGRAMMATION AVEC R POUR L'ANALYSE DE DONNÉES
===
=== LABORATOIRE GESTION DE VERSIONS AVEC GIT
===

CONTENU DE L'ARCHIVE

La présente archive contient les fichiers suivants:

- 'laboratoire-git.pdf', l'énoncé du travail.

- 'validateconf', le fichier de configuration de l'outil de validation
  du système Roger. Consulter le «Guide de validation avec Roger»
  (https://roger-project.gitlab.io/docs/validation/) pour les
  instructions d'utilisation de l'outil.

- LICENSE, le texte de la licence CC BY-SA sous laquelle sont publiés
  l'énoncé et le fichier de configuration.
